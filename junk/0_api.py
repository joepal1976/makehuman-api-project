#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

import gui3d
import mh
import humanmodifier
import gui
import log
import os
from cStringIO import StringIO
from core import G
from codecs import open

class APITaskView(gui3d.TaskView):

    def __init__(self, category):
        mh.api = API()
        gui3d.TaskView.__init__(self, category, 'API')
        box = self.addLeftWidget(gui.GroupBox('Example'))
        self.apiLabel = box.addWidget(gui.TextView('API was loaded'))
        self.human = gui3d.app.selectedHuman




# THIS IS THE ACTUAL API
# All methods here will be available via mh.api.*
class API():

    def __init__(self):
        self.human = gui3d.app.selectedHuman

    def screenShot(self,fileName):
        width = G.windowWidth;
        height = G.windowHeight;
        width = width - 3;
        height = height - 3;
        mh.grabScreen(1,1,width,height,fileName)





category = None
taskview = None

def load(app):
    category = app.getCategory('Utilities')
    taskview = category.addTask(APITaskView(category))

def unload(app):
    pass

