#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

import gui3d
import mh
import gui
import log

class ApiTestView(gui3d.TaskView):

    def __init__(self, category):
        gui3d.TaskView.__init__(self, category, 'Test API')
        box = self.addLeftWidget(gui.GroupBox('Run'))
        self.testButton = box.addWidget(gui.Button('Test'))



        @self.testButton.mhEvent
        def onClicked(event):
            # !!! HERE IS THE API CALL !!!
            mh.api.screenShot("/tmp/hello.png")



apiTestView = None

def load(app):
    global apiTestView
    category = app.getCategory('Utilities')
    apiTestView = category.addTask(ApiTestView(category))

def unload(app):
    pass


