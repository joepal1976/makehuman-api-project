#!/usr/bin/python

import os, sys, json

textPath = None
masterPath = None

if os.path.exists("mhapi-tools/data/namespaces.txt"):
    textPath = "mhapi-tools/data/namespaces.txt"
    masterPath = "mhapi-tools/data/master.json"
else:
    if os.path.exists("data/namespaces.txt"):
        textPath = "data/namespaces.txt"
        masterPath = "data/master.json"

if textPath is None:
    print "Could not find list of namespaces"
    sys.exit(1)

namespaces = []

textfile = open(textPath, "rU")

for name in textfile:
    name = name.strip()
    description = "To be written"
    if "|" in name:
        parts = name.split("|",2);
        name = parts[0].strip()
        description = parts[1].strip()
    ns = dict()
    ns["namespace"] = name
    ns["description"] = description
    ns["calls"] = []
    namespaces.append(ns)

textfile.close()

print json.dumps(namespaces, indent=4, separators=(',', ': '))

