#!/bin/bash

export MHAPI=`pwd`
export PYTHONPATH=$MHAPI:$MHAPI/api

if [ -d "$MHAPI/../makehuman-unstable" ]; then  
  export INSTALL_MH=$MHAPI/../makehuman-unstable/makehuman
else
  export INSTALL_MH=$MHAPI/dist/makehuman
fi

mkdir -p $INSTALL_MH


