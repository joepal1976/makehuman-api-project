#!/usr/bin/python

from api.namespace import NameSpace

class Entities(NameSpace):
    """This namespace contains methods for reaching the most important live objects used in the background code. The calls serve two purposes:

* To be used as an entry point for the *API*. Ie, the rest of the MHAPI will use these calls when implementing operations.
* Allowing users to get direct access to core functionality without having to dig through the core implementations.

Note that the same rule is true for this namespace as for the whole internals hierarchy: You are better off using calls in the 
rest of the API rather than these. Just because you *can* use these does not mean that you *should*."""


    def __init__(self,api):
        """We receive the top level api object so that the full 
        API can be referenced from within this class. For example
        we can here call the "trace" method which is defined in 
        the top level API."""
        self.api = api
        NameSpace.__init__(self)
        self.api.trace()

    def _setupNamespaces(self):
        """This method is defined in the NameSpace object and can be
        overridden if the current namespace has sub-namespaces."""
        self.trace() 
        self.subNames = []



    def checkNamespace(self):
        """We define this method in every namespace just so we can
        check it and crash early if the namespace isn't properly
        configured"""
        self.trace()
        return True

    def shortNameSpaceName(self):
        self.trace() 
        return "entities"

    def fullNameSpaceName(self):
        self.trace() 
        return "self.internals.entities"

    def getApplication(self):
        """Retrieves the MHApplication singleton (commonly called G.app in the background code)."""
        g = self.getGlobals()
        return g.app

    def getGlobals(self):
        """Retrieves the globals object (commonly called G in the background code)."""
        from core import G
        return G

    def getHuman(self):
        """Retrieves the active Human object (commonly called G.app.selectedHuman in the background code)."""
        app = self.getApplication()
        return app.selectedHuman


