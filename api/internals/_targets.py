#!/usr/bin/python

from api.namespace import NameSpace

class Targets(NameSpace):
    """In this namespace you can find calls for working directly with raw targets. Except for fringe cases, this is probably not what you want to do. In order of priority you should 
first look in:

* *self.human.attributes*, which contains high-level manipulators for human physical attributes
* *self.internals.modifiers*, which contains low-level access to functionality for manipulating target sets with dependencies automatically handled.

This said, if you want to *read* information about raw targets, this is probably the right place to start in."""


    def __init__(self,api):
        """We receive the top level api object so that the full 
        API can be referenced from within this class. For example
        we can here call the "trace" method which is defined in 
        the top level API."""
        self.api = api
        NameSpace.__init__(self)
        self.api.trace()

    def _setupNamespaces(self):
        """This method is defined in the NameSpace object and can be
        overridden if the current namespace has sub-namespaces."""
        self.trace() 
        self.subNames = []



    def checkNamespace(self):
        """We define this method in every namespace just so we can
        check it and crash early if the namespace isn't properly
        configured"""
        self.trace()
        return True

    def shortNameSpaceName(self):
        self.trace() 
        return "targets"

    def fullNameSpaceName(self):
        self.trace() 
        return "self.internals.targets"

    def getAppliedTargets(self):
        """Returns information about which targets are applied to the human."""
        self.trace()

    def getExistingTargets(self):
        """Returns information about all exiting targets."""
        self.trace()

    def getTargetWeight(self, fileName):
        """Returns the weight of a target applied to the human."""
        self.trace()

    def saveTarget(self, fileName):
        """Construct a new target from all applied targets and save it as a file."""
        self.trace()

    def setTargetWeight(self, fileName, weight=1.0):
        """Applies a target with a particular weight on the human."""
        self.trace()

    def unsetAllTargets(self):
        """Removes all targets from the list of targets being applied to the human."""
        self.trace()

    def unsetTarget(self, fileName):
        """Removes the target from the list of targets being applied to the human."""
        self.trace()


