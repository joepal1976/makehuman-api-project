#!/usr/bin/python

from namespace import NameSpace

class Gui(NameSpace):
    """To be written"""


    def __init__(self,api):
        """We receive the top level api object so that the full 
        API can be referenced from within this class. For example
        we can here call the "trace" method which is defined in 
        the top level API."""
        self.api = api
        NameSpace.__init__(self)
        self.api.trace()

    def _setupNamespaces(self):
        """This method is defined in the NameSpace object and can be
        overridden if the current namespace has sub-namespaces."""
        self.trace() 
        self.subNames = []

        from gui._widgets import Widgets
        self.widgets = Widgets(self)
        self.subNames.append(self.widgets)
        from gui._tabs import Tabs
        self.tabs = Tabs(self)
        self.subNames.append(self.tabs)
        from gui._events import Events
        self.events = Events(self)
        self.subNames.append(self.events)
        from gui._window import Window
        self.window = Window(self)
        self.subNames.append(self.window)
        from gui._panels import Panels
        self.panels = Panels(self)
        self.subNames.append(self.panels)


    def checkNamespace(self):
        """We define this method in every namespace just so we can
        check it and crash early if the namespace isn't properly
        configured"""
        self.trace()
        return True

    def shortNameSpaceName(self):
        self.trace() 
        return "gui"

    def fullNameSpaceName(self):
        self.trace() 
        return "self.gui"


