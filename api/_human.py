#!/usr/bin/python

from namespace import NameSpace

class Human(NameSpace):
    """This is the root of the section of the namespace which addresses the currently visible human."""


    def __init__(self,api):
        """We receive the top level api object so that the full 
        API can be referenced from within this class. For example
        we can here call the "trace" method which is defined in 
        the top level API."""
        self.api = api
        NameSpace.__init__(self)
        self.api.trace()

    def _setupNamespaces(self):
        """This method is defined in the NameSpace object and can be
        overridden if the current namespace has sub-namespaces."""
        self.trace() 
        self.subNames = []

        from human._hair import Hair
        self.hair = Hair(self)
        self.subNames.append(self.hair)
        from human._topologies import Topologies
        self.topologies = Topologies(self)
        self.subNames.append(self.topologies)
        from human._attributes import Attributes
        self.attributes = Attributes(self)
        self.subNames.append(self.attributes)
        from human._skeleton import Skeleton
        self.skeleton = Skeleton(self)
        self.subNames.append(self.skeleton)
        from human._measurements import Measurements
        self.measurements = Measurements(self)
        self.subNames.append(self.measurements)
        from human._mesh import Mesh
        self.mesh = Mesh(self)
        self.subNames.append(self.mesh)
        from human._posing import Posing
        self.posing = Posing(self)
        self.subNames.append(self.posing)
        from human._skin import Skin
        self.skin = Skin(self)
        self.subNames.append(self.skin)
        from human._bodyparts import Bodyparts
        self.bodyparts = Bodyparts(self)
        self.subNames.append(self.bodyparts)


    def checkNamespace(self):
        """We define this method in every namespace just so we can
        check it and crash early if the namespace isn't properly
        configured"""
        self.trace()
        return True

    def shortNameSpaceName(self):
        self.trace() 
        return "human"

    def fullNameSpaceName(self):
        self.trace() 
        return "self.human"


