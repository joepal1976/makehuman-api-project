#!/usr/bin/python

from namespace import NameSpace

class Internals(NameSpace):
    """The *internals* namespace hierarcy consists of a number of namespaces collecting calls for gaining low-level access to internal MakeHuman functionality.
The idea with these is that you *can* get access to such functionality if you need it, but most definitely not that you *should*.

In the vast majority of cases, you would benefit from first trying to find a relevant call elsewhere in the API, and as a last resort look here."""


    def __init__(self,api):
        """We receive the top level api object so that the full 
        API can be referenced from within this class. For example
        we can here call the "trace" method which is defined in 
        the top level API."""
        self.api = api
        NameSpace.__init__(self)
        self.api.trace()

    def _setupNamespaces(self):
        """This method is defined in the NameSpace object and can be
        overridden if the current namespace has sub-namespaces."""
        self.trace() 
        self.subNames = []

        from internals._targets import Targets
        self.targets = Targets(self)
        self.subNames.append(self.targets)
        from internals._entities import Entities
        self.entities = Entities(self)
        self.subNames.append(self.entities)
        from internals._modifiers import Modifiers
        self.modifiers = Modifiers(self)
        self.subNames.append(self.modifiers)


    def checkNamespace(self):
        """We define this method in every namespace just so we can
        check it and crash early if the namespace isn't properly
        configured"""
        self.trace()
        return True

    def shortNameSpaceName(self):
        self.trace() 
        return "internals"

    def fullNameSpaceName(self):
        self.trace() 
        return "self.internals"


