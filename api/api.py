#!/usr/bin/python

from namespace import NameSpace

class API(NameSpace):
    """The API class is the top level of the api namespace. Every plugin 
    will extend this. The result is that the top level api name space
    will be called "self" within all such plugins.""" 

    def __init__(self,app):
        """Initializes the root of the api hierarcy (and in consequence the rest
        of the whole tree. We require the "app" object to be supplied. This comes from 
        the load() method which is already present in the current plugin
        approach. Each plugin which extends the API base class must 
        remember to call API.__init__(self,app) as the first line in 
        their own __init__() methods.""" 
        self._app = app
        NameSpace.__init__(self)

    def _setupNamespaces(self):
        """This method is defined in the NameSpace object and can be
        overridden if the current namespace has sub-namespaces. 
        Since the current class is the root of the namespace 
        hierarchy, this is true."""
        self.trace() 
        self.subNames = []

        from _files import Files
        self.files = Files(self)
        self.subNames.append(self.files)
        from _gui import Gui
        self.gui = Gui(self)
        self.subNames.append(self.gui)
        from _internals import Internals
        self.internals = Internals(self)
        self.subNames.append(self.internals)
        from _messages import Messages
        self.messages = Messages(self)
        self.subNames.append(self.messages)
        from _scene import Scene
        self.scene = Scene(self)
        self.subNames.append(self.scene)
        from _clothes import Clothes
        self.clothes = Clothes(self)
        self.subNames.append(self.clothes)
        from _human import Human
        self.human = Human(self)
        self.subNames.append(self.human)
        from _environment import Environment
        self.environment = Environment(self)
        self.subNames.append(self.environment)


    def checkNamespace(self):
        """We define this method in every namespace just so we can
        check it and crash early if the namespace isn't properly
        configured. Normally you wouldn't be able to *not* implement it
        since it's defined as an abstract method in the NameSpace class."""
        self.trace()
        return True

    def shortNameSpaceName(self):
        self.trace() 
        return "self"

    def fullNameSpaceName(self):
        self.trace() 
        return "self"

    def subNameSpaces(self):
        self.trace() 
        return self.subNames

    def getBranch(self):
        """Returns the name of the current local code branch, for example 'default'."""
        self.trace()

    def getRevision(self):
        """Return the full textual representation of the Hg revision, for example 'r1604 (d48f36771cc0)'."""
        self.trace()

    def getRevisionNumber(self):
        """Returns the number of the current local revision as an integer, for example 1604."""
        self.trace()

    def getVersion(self):
        """Returns the full textual description of the current version, for example 'MakeHuman unstable 20141120' or 'MakeHuman 1.0.2'."""
        self.trace()

    def getVersionNumberAsArray(self):
        """Returns the numeric representation of the version number as cells in an array, for example [1, 0, 2]."""
        self.trace()

    def getVersionNumberAsString(self):
        """Returns the string representation of the version number, for example '1.0.2'."""
        self.trace()





