#!/usr/bin/python

from api.namespace import NameSpace

class Locations(NameSpace):
    """This namespace collects all calls that deals with finding out the location of important things, such as directories. Here you can 
figure out the location of everything from the user's home to the location where plugin files are read."""


    def __init__(self,api):
        """We receive the top level api object so that the full 
        API can be referenced from within this class. For example
        we can here call the "trace" method which is defined in 
        the top level API."""
        self.api = api
        NameSpace.__init__(self)
        self.api.trace()

    def _setupNamespaces(self):
        """This method is defined in the NameSpace object and can be
        overridden if the current namespace has sub-namespaces."""
        self.trace() 
        self.subNames = []



    def checkNamespace(self):
        """We define this method in every namespace just so we can
        check it and crash early if the namespace isn't properly
        configured"""
        self.trace()
        return True

    def shortNameSpaceName(self):
        self.trace() 
        return "locations"

    def fullNameSpaceName(self):
        self.trace() 
        return "self.environment.locations"

    def getDataDir(self):
        """Return the root location of MakeHuman's data files."""
        self.trace()

    def getPluginsDir(self):
        """Return the location where MakeHuman find plugins."""
        self.trace()

    def getSystemMakeHumanDir(self):
        """Returns the directory in which makehuman is installed. Or in more detail, the directory containing makehuman.py."""
        self.trace()

    def getTempDir(self):
        """Return a directory that the system considers a good place to put temporary files."""
        self.trace()

    def getUserHomeDir(self):
        """Returns the system's idea of what constitutes the user's home directory."""
        self.trace()

    def getUserMakeHumanDir(self):
        """Returns the location of the directory where to store MakeHuman's user preferences and similar."""
        self.trace()


