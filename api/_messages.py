#!/usr/bin/python

from namespace import NameSpace

class Messages(NameSpace):
    """To be written"""


    def __init__(self,api):
        """We receive the top level api object so that the full 
        API can be referenced from within this class. For example
        we can here call the "trace" method which is defined in 
        the top level API."""
        self.api = api
        NameSpace.__init__(self)
        self.api.trace()

    def _setupNamespaces(self):
        """This method is defined in the NameSpace object and can be
        overridden if the current namespace has sub-namespaces."""
        self.trace() 
        self.subNames = []

        from messages._status import Status
        self.status = Status(self)
        self.subNames.append(self.status)
        from messages._logs import Logs
        self.logs = Logs(self)
        self.subNames.append(self.logs)
        from messages._dialogs import Dialogs
        self.dialogs = Dialogs(self)
        self.subNames.append(self.dialogs)


    def checkNamespace(self):
        """We define this method in every namespace just so we can
        check it and crash early if the namespace isn't properly
        configured"""
        self.trace()
        return True

    def shortNameSpaceName(self):
        self.trace() 
        return "messages"

    def fullNameSpaceName(self):
        self.trace() 
        return "self.messages"


