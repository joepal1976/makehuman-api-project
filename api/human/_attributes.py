#!/usr/bin/python

from api.namespace import NameSpace

class Attributes(NameSpace):
    """This namespace wraps calls which deals with changing physical attributes of the human body as such.
Here you will find things for modifying 

* Physical details (such as size of the nose)
* Major characteristics (such as gender, age, ethicity)

The following are found elsewhere though:

* Hair functionality is found in *self.human.hair*
* Skin functionality is found in *self.human.skin*
* Separatly treated, optional and replaceable body parts (such as eyes and genitalia) are found in *self.human.bodyparts*."""


    def __init__(self,api):
        """We receive the top level api object so that the full 
        API can be referenced from within this class. For example
        we can here call the "trace" method which is defined in 
        the top level API."""
        self.api = api
        NameSpace.__init__(self)
        self.api.trace()

    def _setupNamespaces(self):
        """This method is defined in the NameSpace object and can be
        overridden if the current namespace has sub-namespaces."""
        self.trace() 
        self.subNames = []



    def checkNamespace(self):
        """We define this method in every namespace just so we can
        check it and crash early if the namespace isn't properly
        configured"""
        self.trace()
        return True

    def shortNameSpaceName(self):
        self.trace() 
        return "attributes"

    def fullNameSpaceName(self):
        self.trace() 
        return "self.human.attributes"

    def setAge(self, age):
        """Sets the age of the human"""
        self.trace()


