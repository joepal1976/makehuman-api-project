The MakeHuman API Project (MHAPI)
=================================
The makehuman API project is an effort to produce a friendly programming interface for making extensions to MakeHuman. 

This project hasn't really produced anything yet, so there isn't really any useful code. This repo is currently largely
an experimental staging area.

Authoritative reference and discussions
---------------------------------------
The authoritative reference for MHAPI is available in the repo, in the mhapi-tools/data directory. At some point we will make a structured reference and autogenerate it to a readable web format.
Discussions about the API should currently be put somewhere in [the bugtracker](http://bugtracker.makehuman.org/projects/mhapi/issues) or on the [forums](http://forum.makehuman.org/).

More documentation
------------------
There are additional README.md files strategically placed in the subdirs. Check these for more information on how things work. In particulat the README in mhapi-tools should provide some insight.
