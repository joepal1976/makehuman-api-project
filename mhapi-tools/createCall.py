#!/usr/bin/python

import os, sys, json, inspect, imp

from namespace import NameSpace

scriptpath = os.path.abspath(__file__)
basepath = os.path.dirname(scriptpath)
datapath = os.path.abspath(os.path.join(basepath, "data"))
nspath = os.path.abspath(os.path.join(datapath, "namespaces"))
callspath = os.path.abspath(os.path.join(datapath, "calls"))

if len(sys.argv) > 1:
    namespace = sys.argv[1]
else:
    namespace = raw_input("Enter namespace to use, for example self.mynamespace or just self: [self] ")
    if namespace == "":
        namespace = "self"

nsfile = os.path.abspath( os.path.join( nspath, namespace + ".txt" ) )

if not os.path.exists(nsfile):
    print "The namespace " + namespace + " does not seem to exist. The following file is missing: " + nsfile
    sys.exit(1)

nscalldir = os.path.abspath( os.path.join( callspath, namespace ) )
if not os.path.exists(nscalldir):
    os.makedirs(nscalldir)

if len(sys.argv) > 1:
    callname = sys.argv[2]
else:
    callname = raw_input("Enter the name of the new function. Do not include parentheses or parameters: ")
    if callname == "":
        print "Aborted by user."
        sys.exit(1)

callfile = os.path.abspath( os.path.join( nscalldir, callname + ".json" ) )

if os.path.exists(callfile):
    print "The call file already exists."
    sys.exit(1)

callobj = dict()
callobj["name"] = callname;

descr = raw_input("Enter a one-line description of the function: ")
if descr == "":
    print "Aborted by user."
    sys.exit(1)

callobj["description"] = descr

print

print "You will now be asked to enter parameters for the call."
print "If you don't want to enter another parameter, just hit enter."

print

param_name = "!!"

params = []

while not param_name == "":
    param_name = raw_input("Enter the name of the parameter (or hit enter to stop): ")
    if not param_name == "":
        param = dict()
        param["name"] = param_name

        param_desc = raw_input("Enter a one-line description of the parameter: ")
        if param_desc == "":
            param_desc = "To be written"

        param["description"] = param_desc

        param_def = raw_input("Enter default value for the parameter or hit enter for no default value: ")
        
        param["default"] = param_def

        param_type = raw_input("Enter a data type for the parameter. Allowed values are string, float, boolean and integer: [string] ")
        if param_type == "":
            param_type = "string"

        params.append(param)

callobj["parameters"] = params

with open(callfile,'w') as text:
    text.write(json.dumps(callobj, indent=4, separators=(',', ': ')))
    text.close()

print "Wrote " + callfile

