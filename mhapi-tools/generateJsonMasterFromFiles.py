#!/usr/bin/python

import os, sys, json

scriptpath = os.path.abspath(__file__)
basepath = os.path.dirname(scriptpath)
masterfile = os.path.join(basepath,"output","master.json")
nspath = os.path.join(basepath,"data","namespaces")
callpath = os.path.join(basepath,"data","calls")

# print scriptpath
# print basepath
# print nspath
# print callpath

if not os.path.exists(nspath):
    print "The namespaces directory " + nspath + " does not exist."
    sys.exit(1)

namespaces = []
textFiles = {}
rstFiles = {}

for fileName in os.listdir(nspath):    
    fullFileName = os.path.join(nspath,fileName)
    if os.path.isfile(fullFileName) and fileName.startswith("self"):
        name, extension = os.path.splitext(fileName)
        if extension == ".txt":
            namespaces.append(name)
            textFiles[name] = fullFileName
        if extension == ".rst":
            rstFiles[name] = fullFileName

# print namespaces
# print textFiles
# print rstFiles

namespaces.sort()

master = []

for namespace in namespaces:

    json_ns = dict()
    json_ns["namespace"] = namespace

    descr = "to be written"

    print "Parsing " + textFiles[namespace]
    with open(textFiles[namespace],'rU') as text:
        descr = text.read().strip().replace('\n',' ');        
        json_ns["description"] = descr
        text.close()

    docu = descr

    if namespace in rstFiles:
        print "Parsing " + rstFiles[namespace]
        with open(rstFiles[namespace],'rU') as text:
            docu = text.read().strip()
            json_ns["documentation"] = docu
            text.close()
    else:
        json_ns["documentation"] = descr

    calls = []
    callFiles = []

    if os.path.exists(callpath):
        calldir = os.path.join(callpath,namespace)
        if os.path.exists(calldir):
            for fileName in os.listdir(calldir):    
                fullFileName = os.path.join(calldir,fileName).strip()
                name, extension = os.path.splitext(fileName)                
                if os.path.isfile(fullFileName) and extension == ".json":                    
                    callFiles.append(fullFileName)

    for cf in callFiles:
        json_data = open(cf,'rU')
        print "Parsing " + cf
        call = json.load(json_data)
        json_data.close()
        calls.append(call)

    json_ns["calls"] = calls

    master.append(json_ns);

with open(masterfile,'w') as text:
    text.write(json.dumps(master, indent=4, separators=(',', ': ')))
    text.close()

