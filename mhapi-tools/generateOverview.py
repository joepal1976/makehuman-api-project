#!/usr/bin/python

import os, sys, json
from docutils.core import publish_parts

scriptpath = os.path.abspath(__file__)
basepath = os.path.dirname(scriptpath)
masterpath = os.path.join(basepath,"output","master.json")
outputhtml = os.path.join(basepath,"output","overview.html")
outputtxt = os.path.join(basepath,"output","overview.txt")

# print scriptpath
# print basepath
# print nspath
# print callpath

if not os.path.exists(masterpath):
    print "The master file does not exist."
    sys.exit(1)

masterfile = open(masterpath,"rU");
master = json.load(masterfile);
masterfile.close();


namespace_list = []
namespaces = {}

for ns in master:
    name = ns["namespace"];
    namespace_list.append(name);
    namespaces[name] = ns;

namespace_list.sort()

with open(outputhtml,"w") as output:
    output.write("<html>\n  <head>\n    <title>Namespace overview</title>\n")
    output.write('    <link rel="stylesheet" type="text/css" href="overview.css" />\n  </head>\n  <body>\n');

    output.write("    <h1>List of available namespaces</h1>\n    <ul>\n")

    for ns in namespace_list:
        output.write("      <li><a href=\"#" + ns + "\">" + ns + "</a> -- <i>" + namespaces[ns]["description"] + "</i></li>\n");

    output.write("    </ul>\n")

    for ns in namespace_list:
        output.write("  <a name=\"" + ns + "\"></a>\n")
        output.write("  <h2>" + ns + "</h2>\n")
        html = publish_parts(namespaces[ns]["documentation"], writer_name='html')['html_body']
        output.write("  <p>" + html + "</p>\n")

        if len(namespaces[ns]["calls"]) > 0:
            for call in namespaces[ns]["calls"]:
                output.write("  <h3>" + call["name"] + "(");
                parano = 0;
                for para in call["parameters"]:
                    if parano > 0:
                        output.write(", ")
                    parano = parano + 1
                    output.write(para["name"]);
                    if not para["default"] == "":
                        output.write("=" + para["default"])

                output.write(")</h3>\n");
                output.write("  <p>" + call["description"] + "</p>\n");
                output.write("  <p><b>Parameters:</b></p>\n  <ul>\n");
                for para in call["parameters"]:
                    output.write("    <li><i>" + para["name"] + "</i>&nbsp;: ");
                    output.write(para["description"] + "</li>\n");

                output.write("  </ul>\n");

                if "returns" in call:
                    output.write("<p><b>Returns:</b></p>\n  <ul>\n")
                    output.write("    <li>" + call["returns"] + "</li>")
                    output.write("  </ul>\n")


    output.write("  <body>\n</html>");
    output.close()

with open(outputtxt,"w") as output:
    output.write("List of available namespaces\n")
    output.write("----------------------------\n\n")

    for ns in namespace_list:
        output.write(ns + ": " + namespaces[ns]["description"] + "\n");
        if len(namespaces[ns]["calls"]) > 0:
            for call in namespaces[ns]["calls"]:
                output.write("     -- " + call["name"] + "(");
                parano = 0;
                for para in call["parameters"]:
                    if parano > 0:
                        output.write(", ")
                    parano = parano + 1
                    output.write(para["name"]);
                    if not para["default"] == "":
                        output.write("=" + para["default"])

                output.write(")\n");
    output.close()

