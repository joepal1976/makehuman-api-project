#!/usr/bin/python

import os, sys, json, shutil, re
from string import Template

scriptpath = os.path.abspath(__file__)
basepath = os.path.dirname(scriptpath)
masterpath = os.path.join(basepath,"output","master.json")
rootpath = os.path.abspath(os.path.join(basepath, os.pardir))
datapath = os.path.abspath(os.path.join(basepath, "data"))
blenderpath = os.path.join(datapath,"blender")
callspath = os.path.abspath(os.path.join(datapath, "calls"))
distdir = os.path.join(rootpath,"dist")

classtmpl = os.path.join(datapath,"blender_class.py")
selftmpl = os.path.join(datapath,"blender_self.py")
nsfile = os.path.join(datapath,"blender_namespace.py")

self_file = open( selftmpl )
self_template = Template( self_file.read() )
self_file.close()

class_file = open( classtmpl )
class_template = Template( class_file.read() )
class_file.close()

if not os.path.exists(distdir):
    os.makedirs(distdir)

blenderdir = os.path.join(distdir,"blender")

if not os.path.exists(blenderdir):
    os.makedirs(blenderdir)

apidir = os.path.join(blenderdir,"api")

if not os.path.exists(apidir):
    os.makedirs(apidir)

if not os.path.exists(masterpath):
    print "The master file does not exist."
    sys.exit(1)

masterfile = open(masterpath,"rU");
master = json.load(masterfile);
masterfile.close();

namespace_list = [
        "self",
        "self.environment",
        "self.environment.locations"
        ]

namespaces = {}


for ns in master:
    name = ns["namespace"];
    #print name
    #namespace_list.append(name);
    namespaces[name] = ns;
    namespaces[name]["subs"] = dict()

namespace_list.sort()

for ns in namespace_list:
    print ns
    namespace = namespaces[ns]

    if not ns == "self":
        dots = ns.split(".")
        last = len(dots) - 1
        shortname = dots[last]
        dots.pop()
        parentname = ".".join(dots)
        # print ns + " " + parentname + " " + shortname
        parent = namespaces[parentname]
        parent["subs"][ns] = shortname

for ns in namespace_list:
    namespace = namespaces[ns]

    d = dict()
    if not ns == "self":
        d["fullname"] = ns 
        dots = ns.split(".")
        dlast = len(dots) - 1
        d["shortname"] = dots[dlast]
        d["capname"] = d["shortname"].capitalize()
        namespace["template"] = class_template
        dots[0] = apidir
        namespace["subdir"] = os.path.abspath( os.path.join(*dots) )
        namespace["dir"] = os.path.abspath(os.path.join(namespace["subdir"], os.pardir))
        namespace["filename"] = os.path.join( namespace["dir"], "_" + d["shortname"] + ".py")
        d["documentation"] = '    """' + namespace["documentation"] + '"""' + "\n"        
        d["nsimport"] = "namespace"
        if len(dots) > 2:
            d["nsimport"] = "api.namespace"
        dots.pop(0)
        namespace["imports"] = ".".join(dots) + "."
    else:
        namespace["template"] = self_template
        namespace["dir"] = os.path.abspath( apidir )
        namespace["subdir"] = os.path.abspath( apidir )
        namespace["filename"] = os.path.join(namespace["dir"], "api.py")
        namespace["imports"] = ""

    callstr = ""
    for call in namespace["calls"]:
        callstr = callstr + "    def "
        callstr = callstr + call["name"] + "(self"
        for p in call["parameters"]:
            callstr = callstr + ", "
            callstr = callstr + p["name"]
            if not p["default"] == "":
                callstr=callstr+"=" + p["default"]
        callstr = callstr + "):\n"
        callstr = callstr + '        """' + call["description"] + '"""' + "\n"
        callstr = callstr + "        self.trace()\n\n"
        
    d["methods"] = callstr
   
    substr = ""
    for s in namespace["subs"].keys():
        subname = namespace["subs"][s]
        substr = substr + "        from " + namespace["imports"] + "_" + subname + " import " + subname.capitalize() + "\n"
        substr = substr + "        self." + subname + " = " + subname.capitalize() + "(self)\n"
        substr = substr + "        self.subNames.append(self." + subname + ")\n"

    d["subnames"] = substr

    sublen = len( namespace["subs"].keys() )
    if sublen > 0:
        if not os.path.exists( namespace["subdir"] ):
            os.makedirs(namespace["subdir"])
        if not ns == "self":
            with open(os.path.join(namespace["subdir"],"__init__.py"),"w") as init_file:
                init_file.write("#!/usr/bin/python\n\n")
                init_file.close()

    full_file = namespace["template"].substitute(d);

    print namespace["filename"]

    with open(namespace["filename"],"w") as class_file:
        class_file.write(full_file)
        class_file.close()


nsout = os.path.join(apidir,"namespace.py")

shutil.copy(nsfile,nsout)

files = [f for f in os.listdir(blenderpath) if re.match(r'.*\.py$', f)]

for f in files:
    inf = os.path.join(blenderpath,f)
    outf = os.path.join(blenderdir,f)
    shutil.copy(inf,outf)

