#!/usr/bin/python

import os, sys, json, inspect, imp

from namespace import NameSpace

scriptpath = os.path.abspath(__file__)
basepath = os.path.dirname(scriptpath)
masterpath = os.path.join(basepath,"output","master.json")
rootpath = os.path.abspath(os.path.join(basepath, os.pardir))
datapath = os.path.abspath(os.path.join(basepath, "data"))
nspath = os.path.abspath(os.path.join(datapath, "namespaces"))
nsfile = os.path.abspath(os.path.join(datapath, "namespaces.txt"))
callspath = os.path.abspath(os.path.join(datapath, "calls"))
apidir = os.path.abspath(os.path.join(rootpath,"api"))

doExtract = False
forceExtract = False

if len(sys.argv) > 1:
    if sys.argv[1] == "--extract":
        doExtract = True
    if sys.argv[1] == "--force":
        doExtract = True
        forceExtract = True

if len(sys.argv) > 2:
    if sys.argv[2] == "--extract":
        doExtract = True
    if sys.argv[2] == "--force":
        doExtract = True
        forceExtract = True

if not os.path.exists(masterpath):
    print "The master file does not exist."
    sys.exit(1)

masterfile = open(masterpath,"rU");
master = json.load(masterfile);
masterfile.close();

namespace_list = []
namespaces = {}

for ns in master:
    name = ns["namespace"];
    namespace_list.append(name);
    namespaces[name] = ns;

namespace_list.sort()

namespace_api = []
api_files = dict()

namespace_api.append("self");
api_files["self"] = os.path.abspath( os.path.join( apidir, "api.py" ) )

for root, dirs, files in os.walk(apidir):
    current = root.replace(apidir,"")
    if current.startswith(os.sep):
        current = current.replace(os.sep,"",1)

    parts = current.split(os.sep)

    if current == "":
        nsroot = "self"
    else:
        nsroot = ".".join(parts)
        nsroot = "self." + nsroot

    for name in files:
        fileName, fileExtension = os.path.splitext( os.path.basename(name) )
        if fileExtension == ".py":
            if not fileName == "api" and not fileName == "namespace" and not fileName == "__init__":
                fn = fileName.replace("_","",1)
                ns = nsroot + "." + fn
                namespace_api.append(ns)
                api_files[ns] = os.path.abspath( os.path.join(root, name) )

namespace_api.sort()

print 
print "NAMESPACES IN DEFINITION BUT NOT IN API FILES:"
print "----------------------------------------------"

for ns in namespace_list:
    if not ns in namespace_api:
        print ns + " (" + api_files[ns] + ")"

print 
print "NAMESPACES IN API FILES BUT NOT IN DEFINITION:"
print "----------------------------------------------"

extractNs = False;

for ns in namespace_api:
    if not ns in namespace_list:
        print ns + " was not found in definition, but exists in API"
        nsdef = os.path.join(nspath,ns + ".txt")
        if doExtract:
            with open(nsfile,"a") as nsf:
                nsf.write(ns + "\n")
            with open(nsdef,"w") as nsf:
                nsf.write("To be written\n")
            print "Wrote definition for " + ns
            extractNs = True

if extractNs:
    print "\nThe namespace collection was modified. This script will now terminate."
    print "You should run generateJsonMasterFromFiles.py before running thie script again."
    sys.exit(0)

namespace_class = dict()

foo = imp.load_source("api", os.path.join(apidir,"api.py") )
imp.load_source("api.namespace", os.path.join(apidir,"namespace.py") )

namespace_abstract_methods = ["__init__","_setupNamespaces","checkNamespace","fullNameSpaceName","shortNameSpaceName","subNameSpaces","trace"]

for ns in namespace_api:
    namespace_class[ns] = dict()
    namespace_class[ns]["namespace"] = ns

    calls = []

    foo = imp.load_source(ns, api_files[ns] )

    foo_1_name = str(inspect.getmembers(foo,inspect.isclass)[0][0])
    foo_1_class = inspect.getmembers(foo,inspect.isclass)[0][1]

    foo_2_name = str(inspect.getmembers(foo,inspect.isclass)[1][0])
    foo_2_class = inspect.getmembers(foo,inspect.isclass)[1][1]

    #print str(foo_1) + " -- " + str(foo_2)

    if foo_1_name == "NameSpace":
        foo_class = foo_2_class
    else:
        foo_class = foo_1_class

    # print "\n" + ns + " -- " + str( foo_class )

    if not foo_class.__doc__ is None:
        namespace_class[ns]["documentation"] = foo_class.__doc__

    methods = inspect.getmembers(foo_class,inspect.ismethod)

    for m in methods:
        method = dict()        
        method_name = str(m[0])

        method["name"] = method_name;
        params = [];

        if method_name not in namespace_abstract_methods:

            call = getattr(foo_class,method_name)
            func = call.im_func

            if not func.__doc__ is None:
                method["description"] = func.__doc__

            gas = inspect.getargspec(func)

            args = gas.args
            args.pop(0)
            alen = len(args)

            defs = gas.defaults

            revdefs = []

            for i in range(0,alen):
                revdefs.append("")

            if not defs is None:
                for i in range(0,len(defs)):
                    revdefs[ len(revdefs) - i - 1 ] = str(defs[i])

            for i in range(0,alen,1):
                param = dict()
                param["name"] = args[i]
                param["default"] = revdefs[i]

                #print method_name + " -- " + param["name"] + " -- " + str(param["default"])

                params.append(param)

            method["parameters"] = params

            lst = (inspect.getsourcelines(m[1]))[0]
            method["source"] = "".join(lst)

            calls.append(method)            

    namespace_class[ns]["calls"] = calls

print 
print "METHOD DIFFERENCES:"
print "-------------------"

notifyExtract = False;

for ns in namespace_api:
    ns_api = namespace_class[ns]
    nsname = ns_api["namespace"]
    callsdir = os.path.join(callspath,ns)

    calls_api = dict()
    calls_master = dict()

    if ns in namespace_list:
        ns_master = namespaces[ns]
        for c in ns_api["calls"]:
            calls_api[c["name"]] = c
        for c in ns_master["calls"]:
            calls_master[c["name"]] = c

    keys = calls_api.keys()
    for m in keys:
        if m not in calls_master:
            print nsname + "." + m + "() exists in API but not in master.json"
            print "---"
            print json.dumps(calls_api[m],indent=4)
            print "---"
            if doExtract:
                impl = os.path.join(callsdir,m + ".py")
                jsondef = os.path.join(callsdir,m + ".json")
                if not os.path.exists(callsdir):
                    os.makedirs(callsdir)
                if not os.path.exists(impl) or forceExtract:
                    print "extracting " + nsname + "." + m
                    with open(impl, "w") as text_file:
                        text_file.write(method_api["source"])
                    print "Wrote " + impl 
                if not os.path.exists(jsondef) or forceExtract:
                    with open(jsondef, "w") as text_file:
                        obj = calls_api[m]
                        del obj["source"]
                        text_file.write(str(json.dumps(obj,indent=4)))
                    print "Wrote " + jsondef
            del calls_api[m] 

    keys = calls_master.keys()
    for m in keys:
        if m not in calls_api:
            print nsname + "." + m + "() exists in master.json but not in API"
            print "---"
            print json.dumps(calls_master[m],indent=4)
            print "---"
            del calls_master[m]
            
    keys = calls_api.keys()

    for m in keys:
        method_api = calls_api[m]
        method_master = calls_master[m]        
        
        method_api_source = method_api["source"]

        impl = os.path.join(callsdir,m + ".py")

        parameters_api = method_api["parameters"]
        parameters_master = method_master["parameters"]

        if not len(parameters_api) == len(parameters_master):
            print nsname + "." + m + " has a different number of arguments between master.json (" + str(len(parameters_master)) + ") and the API (" + str(len(parameters_api)) + ")"
            capi = ""
            for c in parameters_api:
                capi = capi + " " + c["name"]
            cmaster = ""
            for c in parameters_master:
                cmaster = cmaster + " " + c["name"]

            print "---"
            print "  in master:" + cmaster
            print "  in api   :" + capi
            print "---"

        if not os.path.exists(impl):
            # print nsname + "." + m + " does not have an extracted implementation. Skipping source comparison."
            if not doExtract:
                notifyExtract = True
            else:
                print "extracting " + nsname + "." + m
                with open(impl, "w") as text_file:
                    text_file.write(method_api["source"])
                print "Wrote " + impl 
        else:
            with open (impl, "r") as impfile:
                method_ext_source = impfile.read()

            if method_ext_source != method_api_source:                
                print "The source of " + nsname + "." + m + " differs from the extracted implementation.\n"
                print "--- in the api dir:"
                print method_api_source
                print "--- in the extracted implementation:"
                print method_ext_source
                print
                if forceExtract:
                    print "Overwriting existing extraction of " + nsname + "." + m
                    with open(impl, "w") as text_file:
                        text_file.write(method_api["source"])
                    print "Wrote " + impl 
                else:
                    print "(skipping extraction, since file exists. Override with --force)"


if notifyExtract:
    print
    print "Some methods did not have an extracted implementation."
    print "Run this script with argument --extract if you wish to extract implementations."
    print "Add --force if you also want to overwrite existing extracted implementations."

