Tools
=====
This directory contains tools for creating, working with and checking the API.

Everything here presumes that you have sensible contents in the PYTHONPATH variable. At least the
"api" directory should be referenced (via an absolute path) in the PYTHONPATH variable.

Definition files
----------------
The "data" directory contains the formal definition of the API. Here you'll find two major subdirs:

* "namespaces" which contains the definition of the hierarchy as such
* "calls" which contains function definitions for each leaf in the hierarchy

To get more information about the format of these directories, check the README.md files in each subdir.

master.json
-----------
Most scripts work through a file called "output/master.json" which is generated from 
the files in the "data" directory. 

If you changed anything under "data/namespaces" or "data/calls", you should run 

    python generateJsonMasterFromFiles.py
    
in order to generate a new "output/master.json"

Generating HTML documentation
-----------------------------
To generate HTML-formatted documentation about the API, run 

    python generateOverview.py
    
This will create documentation in the "output" subdir

Wiping the API hierarchy and generating a module outline
--------------------------------------------------------
This REALLY WILL WIPE the api directory and overwrite everything. You have been warned.

If you want to create a structured module hierarcy with appropriately named python module files and stubs for all the calls, use 

    python wipe.py
    
This will use the definition in "master.json" and create everything you need under "api". BUT ALL EXISTING CODE WILL BE REMOVED.

Creating a new call definition
------------------------------
If you want to create a call (ie a function) inside a namespace, use the aptly named script "createCall", with the syntax:

    python createCall.py [namespace] [function name]

If you don't supply the namespace and name parameters, you will be asked for them. The script will ask you all relevant 
questions and then create a json file with the call definition in the correct place.

Checking for changes without destroying code
--------------------------------------------
If you have edited the API files in order to, for example, implement actual code in the calls, it will no longer make sense
to keep wiping and redoing from scratch. Instead you can run

    python compareMasterWithApi.py

This will list inconsistencies between the master.json file and how the code looks under the API directory.

Currently, the script will not help you fix these things. This might change in the future.

You will want to run this script before committing any change to the repo, just to check that you didn't miss anything.

Checking the hierarchy
----------------------
Once you have generated and/or worked with the API hierarchy, you can do basic testing with 

    python checkBasicSanity.py
    
This will call the API as if from a plugin written for it. If it doesn't crash, things are probably reasonably ok.
