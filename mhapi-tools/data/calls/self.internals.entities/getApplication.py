    def getApplication(self):
        """Retrieves the MHApplication singleton (commonly called G.app in the background code)."""
        g = self.getGlobals()
        return g.app
