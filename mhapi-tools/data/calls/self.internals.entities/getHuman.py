    def getHuman(self):
        """Retrieves the active Human object (commonly called G.app.selectedHuman in the background code)."""
        app = self.getApplication()
        return app.selectedHuman
