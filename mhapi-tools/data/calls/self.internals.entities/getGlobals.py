    def getGlobals(self):
        """Retrieves the globals object (commonly called G in the background code)."""
        from core import G
        return G
