    def getVersion(self):
        """Returns the full textual description of the current version, for example 'MakeHuman unstable 20141120' or 'MakeHuman 1.0.2'."""
        self.trace()
