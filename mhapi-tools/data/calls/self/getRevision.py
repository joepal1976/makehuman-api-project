    def getRevision(self):
        """Return the full textual representation of the Hg revision, for example 'r1604 (d48f36771cc0)'."""
        self.trace()
