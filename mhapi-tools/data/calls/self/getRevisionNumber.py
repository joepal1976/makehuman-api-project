    def getRevisionNumber(self):
        """Returns the number of the current local revision as an integer, for example 1604."""
        self.trace()
