    def getVersionNumberAsArray(self):
        """Returns the numeric representation of the version number as cells in an array, for example [1, 0, 2]."""
        self.trace()
