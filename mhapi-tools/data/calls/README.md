Call definitions
================
This directory contains all calls available for the MHAPI. Each call is a json file containing the function name, a doc string
and definitions of the available parameters.

Creating a new call
-------------------
The easiest way to create a new call is by:

* Create a subdir for the namespace if it does not already exist
* Copy an existing call, for example from the self.internals.targets subdir
* Rename the file to match the exact name of your call
* Edit the file so that it contains the correct definition

Moving a call to another namespace
----------------------------------
If you realize a call should have been places somewhere else in the namespace
hierarchy, simply move the corresponding json file to the currect subdir.

Applying changes
----------------
Nothing here has any effect until you have generated a new "master.json" file. See 
readme in the tools directory for information on this.

