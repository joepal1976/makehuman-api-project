#!/usr/bin/python

from namespace import NameSpace

class $capname(NameSpace):
$documentation

    def __init__(self,api):
        """We receive the top level api object so that the full 
        API can be referenced from within this class. For example
        we can here call the "trace" method which is defined in 
        the top level API."""
        self.api = api
        NameSpace.__init__(self)
        self.api.trace()

    def _setupNamespaces(self):
        """This method is defined in the NameSpace object and can be
        overridden if the current namespace has sub-namespaces."""
        self.trace() 
        self.subNames = []

$subnames

    def checkNamespace(self):
        """We define this method in every namespace just so we can
        check it and crash early if the namespace isn't properly
        configured"""
        self.trace()
        return True

    def shortNameSpaceName(self):
        self.trace() 
        return "$shortname"

    def fullNameSpaceName(self):
        self.trace() 
        return "$fullname"

$methods

    def getLiveVertices(self,obj):
        return [vertex.co for vertex in obj.data.vertices]
    
    def getJsonVertices(self,obj):
        verts = MHAPI_GetLiveVertices(obj)
        data = []
        for vert in verts:
            v = [vert[0],vert[1],vert[2]]
            data.append(v)
        return data
    
    def putJsonVertices(self,jsondata,obj):
        livedata = GetLiveVertices(obj)
    
        l = len(jsondata)
    
        for i in range(l):
            livedata[i][0] = jsondata[i][0]
            livedata[i][1] = jsondata[i][1]
            livedata[i][2] = jsondata[i][2]
    
    def executeJsonCall(self,jsoncall):
        
        jsondata = json.dumps(jsoncall).encode("utf-8")
    
        client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client.connect(('127.0.0.1', 6789))
        client.send(jsondata)
     
        data = ""
    
        while True:
            buf = client.recv(1024)
            if len(buf) > 0:
                data += buf.strip().decode('utf-8')
            else:
                break
    
        return data;
    
    def constructJsonCall(self,functionName):
        jsondata = {}
        jsondata["functionName"] = functionName
        jsondata["arguments"] = {}
    
        return jsondata
    
    def appendJsonArgument(self,jsonCall,argumentName,argumentData):
        jsonCall["arguments"][argumentName] = argumentData


