#!/usr/bin/python
# -*- coding: utf-8 -*-

import json
import socket

def GetLiveVertices(obj):
    return [vertex.co for vertex in obj.data.vertices]

def GetJsonVertices(obj):
    verts = MHAPI_GetLiveVertices(obj)
    data = []
    for vert in verts:
        v = [vert[0],vert[1],vert[2]]
        data.append(v)
    return data

def PutJsonVertices(jsondata,obj):
    livedata = GetLiveVertices(obj)

    l = len(jsondata)

    for i in range(l):
        livedata[i][0] = jsondata[i][0]
        livedata[i][1] = jsondata[i][1]
        livedata[i][2] = jsondata[i][2]

def ExecuteJsonCall(jsoncall):
    
    jsondata = json.dumps(jsoncall).encode("utf-8")

    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client.connect(('127.0.0.1', 6789))
    client.send(jsondata)
 
    data = ""

    while True:
        buf = client.recv(1024)
        if len(buf) > 0:
            data += buf.strip().decode('utf-8')
        else:
            break

    return data;

def ConstructJsonCall(functionName):
    jsondata = {}
    jsondata["functionName"] = functionName
    jsondata["arguments"] = {}

    return jsondata

def AppendJsonArgument(jsonCall,argumentName,argumentData):
    jsonCall["arguments"][argumentName] = argumentData


