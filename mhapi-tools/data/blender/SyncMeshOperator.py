#!/usr/bin/python
# -*- coding: utf-8 -*-

bl_info = {
    "name": "Synchronize MakeHuman mesh",
    "category": "Mesh",
}

import bpy
import json
import pprint

from api import MHAPI

pp = pprint.PrettyPrinter(indent=4)

class SyncMHMeshOperator(bpy.types.Operator):
    """Synchronize the shape of a human with MH"""
    bl_idname = "mesh.sync_mh_mesh"
    bl_label = "Synchronize MH Mesh"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        print("Execute syncmesh")
        
        obj = context.object
        if obj is None or not obj.type == 'MESH':
            self.report({'ERROR'}, "Must select mesh to synchronize")
        else:
            
            c = MHAPI.constructJsonCall("getMesh")
            MHAPI.appendJsonArgument(c,"hej","hopp")

            json_raw = MHAPI.executeJsonCall(c)
            print(json_raw)
            json_obj = json.loads(json_raw)

            #mhapi.PutJsonVertices(json_obj,obj) 
            
        # The original script
        #scene = context.scene
        #for obj in scene.objects:
        #    obj.location.x += 1.0


        return {'FINISHED'} 
