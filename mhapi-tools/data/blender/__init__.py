#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys, os

bl_info = {
    "name": "MHAPI",
    "author": "Joel Palmius",
    "version": (0, 1),
    "blender": (2, 6, 73),
    "location": "View3D > Properties > MHAPI",
    "description": "MHAPI",
    "warning": "",
    'wiki_url': "https://bitbucket.org/joepal1976/makehuman-api-project",
    "category": "MakeHuman"}


if "bpy" in locals():
    print("Reloading MHAPI")
    import imp
    #imp.reload(MHAPI_Functions)
    imp.reload(SyncMeshOperator)
    imp.reload(TestPanel)
    imp.reload(SyncPanel)
else:
    print("Loading MHAPI")

    loc = os.path.dirname(os.path.abspath(__file__))
    syspath = [ os.path.join(loc,"api") ]
    syspath.extend(sys.path)
    sys.path = syspath

    #from . import MHAPI_Functions
    from . import SyncMeshOperator
    from . import TestPanel
    from . import SyncPanel

    from api import MHAPI

import bpy
from bpy.props import *
from bpy_extras.io_utils import ImportHelper, ExportHelper


def register():
    bpy.utils.register_module(__name__)
    #bpy.utils.register_class(MHAPI_SyncPanel)

def unregister():
    bpy.utils.unregister_module(__name__)
    #bpy.utils.unregister_class(MHAPI_SyncPanel)

if __name__ == "__main__":
    register()


