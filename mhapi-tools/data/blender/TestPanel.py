#!/usr/bin/python
# -*- coding: utf-8 -*-

import bpy
import os
from bpy.props import *
from bpy_extras.io_utils import ImportHelper, ExportHelper


class MHAPI_TestPanel(bpy.types.Panel):
    bl_label = "MHAPI testpanel"
    bl_space_type = "VIEW_3D"
    bl_region_type = "TOOLS"
    bl_category = "MHAPI"

    def draw(self, context):
        layout = self.layout
        row = layout.row()
        row.label(text="Add Objects:")
        split = layout.split()
        col = split.column(align=True)
        col.operator("mesh.primitive_plane_add", text="Plane", icon='MESH_PLANE')
        col.operator("mesh.primitive_torus_add", text="Torus", icon='MESH_TORUS')

