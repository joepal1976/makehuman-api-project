#!/usr/bin/python

from namespace import NameSpace

class API(NameSpace):
    """The API class is the top level of the api namespace. Every plugin 
    will extend this. The result is that the top level api name space
    will be called "self" within all such plugins.""" 

    def __init__(self):
        """Initializes the root of the api hierarcy (and in consequence the rest
        of the whole tree. We require the "app" object to be supplied. This comes from 
        the load() method which is already present in the current plugin
        approach. Each plugin which extends the API base class must 
        remember to call API.__init__(self) as the first line in 
        their own __init__() methods.""" 
        NameSpace.__init__(self)

    def _setupNamespaces(self):
        """This method is defined in the NameSpace object and can be
        overridden if the current namespace has sub-namespaces. 
        Since the current class is the root of the namespace 
        hierarchy, this is true."""
        self.trace() 
        self.subNames = []

$subnames

    def checkNamespace(self):
        """We define this method in every namespace just so we can
        check it and crash early if the namespace isn't properly
        configured. Normally you wouldn't be able to *not* implement it
        since it's defined as an abstract method in the NameSpace class."""
        self.trace()
        return True

    def shortNameSpaceName(self):
        self.trace() 
        return "self"

    def fullNameSpaceName(self):
        self.trace() 
        return "self"

    def subNameSpaces(self):
        self.trace() 
        return self.subNames

$methods

if not "MHAPI" in locals():
    print("Creating MHAPI")
    MHAPI = API()

