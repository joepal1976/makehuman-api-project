In this namespace you can find calls for working directly with raw targets. Except for fringe cases, this is probably not what you want to do. In order of priority you should 
first look in:

* *self.human.attributes*, which contains high-level manipulators for human physical attributes
* *self.internals.modifiers*, which contains low-level access to functionality for manipulating target sets with dependencies automatically handled.

This said, if you want to *read* information about raw targets, this is probably the right place to start in.

