This namespace wraps calls which deals with changing physical attributes of the human body as such.
Here you will find things for modifying 

* Physical details (such as size of the nose)
* Major characteristics (such as gender, age, ethicity)

The following are found elsewhere though:

* Hair functionality is found in *self.human.hair*
* Skin functionality is found in *self.human.skin*
* Separatly treated, optional and replaceable body parts (such as eyes and genitalia) are found in *self.human.bodyparts*.

