The *internals* namespace hierarcy consists of a number of namespaces collecting calls for gaining low-level access to internal MakeHuman functionality.
The idea with these is that you *can* get access to such functionality if you need it, but most definitely not that you *should*.

In the vast majority of cases, you would benefit from first trying to find a relevant call elsewhere in the API, and as a last resort look here.

