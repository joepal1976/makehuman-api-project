This is the root of the namespace hierarcy. In practise, calls here will end up being callable 
from "self" in your plugin, as if they were a part of the class you are writing.

Calls in this namespace fall into two categories:

* Information about MakeHuman (such as version)
* Calls that deal with managing/querying MHAPI as such

