This namespace contains methods for reaching the most important live objects used in the background code. The calls serve two purposes:

* To be used as an entry point for the *API*. Ie, the rest of the MHAPI will use these calls when implementing operations.
* Allowing users to get direct access to core functionality without having to dig through the core implementations.

Note that the same rule is true for this namespace as for the whole internals hierarchy: You are better off using calls in the 
rest of the API rather than these. Just because you *can* use these does not mean that you *should*.
