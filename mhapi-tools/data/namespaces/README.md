When using scripts to create an empty namespace hierarchy from scratch,
this is the authoritative place for the definition. 

In this directory you will (or should) find two files per part of the
namespace: 

* one with extension "txt", which is the short description of the name
  space. This is what ends up in lists and links in documentation.
* one with extension "rst", which is the sphinx inline documentation
  of the module created to wrap the namespace. This one is optional.
  if it doesn't exist, the txt file will be used here too.

If a rst file exists where a txt does not, this will be ignored.


