This namespace collects all calls that deals with finding out the location of important things, such as directories. Here you can 
figure out the location of everything from the user's home to the location where plugin files are read.
