#!/usr/bin/python

# This script will just import the api and make a trial run of the
# example plugin. This should probably be extended in the future.

import os

from api.api import API

scriptpath = os.path.abspath(__file__)
basepath = os.path.dirname(scriptpath)
rootpath = os.path.abspath(os.path.join(basepath, os.pardir))
pluginpath = os.path.join(rootpath,"plugins","7_api_test.py")

print "\n### --- NOTES\n"

print "If things crash unexpectedly, check that you added the full path to the"
print "\"api\" subdirectory to PYTHONPATH. This is not done automatically.\n"

print "### --- BEGINNING SANITY CHECK OF API AS SUCH\n"

test = API(None)

print "\nIf we got this far, the sanity check passed."

print "\n### --- TRYING TO DRY-RUN EXECUTE EXAMPLE PLUGIN\n"

execfile(pluginpath)

print "\nIf we got this far, the sanity check passed.\n"

