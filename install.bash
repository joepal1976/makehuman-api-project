#!/bin/bash

. setupEnvironment.bash

cd $INSTALL_MH

rm -rf api
rm -rf plugins/*mhapi*

cp -rfv $MHAPI/api api
cp -rfv $MHAPI/plugins/* plugins/

