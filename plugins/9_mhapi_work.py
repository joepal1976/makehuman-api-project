#!/usr/bin/python

from api.api import API

import gui3d
import mh
import gui
import log

class mhapi_work(API):

    def __init__(self,app):    # These two exact lines must exist in all API plugins
        API.__init__(self,app) # 

        app = self.internals.entities.getApplication()

        cat = app.getCategory('MHAPI')
        view = gui3d.TaskView(cat,"Work");
        cat.addTask(view)

        print view

me = None

def load(app):
    global me
    me = mhapi_work(app)

def unload(app):
    pass

me = mhapi_work(None)


