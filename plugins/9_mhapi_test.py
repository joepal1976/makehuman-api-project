#!/usr/bin/python

# Only thing we need to import will usually be the API superclass
from api.api import API

# All API-style plugins are descendants of the API class
class api_test(API):

    def __init__(self,app):    # These two exact lines must exist in all API plugins
        API.__init__(self,app) # 

        # At this point "self" corresponds to the API namespace, so that
        # self.xxxx() is a top-level method, and self.human.xxxx() is a method
        # in the "human" sub namespace

        # --- From here on is the plugin functionality ---

        # checkNamespace() should be defined in each namespace. It's a dummy method
        # which simply returns True. This way we will crash early if the namespace
        # isn't available.
        if self.checkNamespace():
            print "The top level namepace is available"
        if self.human.checkNamespace():
            print "The \"human\" namespace is available"

        # --- ACTUAL EXAMPLE OF HOW A CALL WOULD LOOK ---
        self.human.attributes.setAge(0.5)


# --- EVERYTHING BELOW THIS POINT REGARDS TRADITIONAL PLUGIN INSTANTIATION ---

# Note that in this example we simply check that the API is available, so there is nothing
# regarding UI implemented anywhere

me = None

# This method is called when the plugin is loaded into makehuman
# The app reference is passed so that a plugin can attach a new category, task, or other GUI elements
def load(app):
    global me
    me = api_test(app)

# This method is called when the plugin is unloaded from makehuman
# At the moment this is not used, but in the future it will remove the added GUI elements
def unload(app):
    pass


# Uncomment this if testing API outside scope of MH
# (it does not hurt to keep it uncommented even when running inside MH)
me = api_test(None)

